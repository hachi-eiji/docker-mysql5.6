FROM circleci/mysql:5.6

RUN echo '\n\
[mysqld]\n\
innodb_file_format = Barracuda\n\
innodb_file_per_table = 1\n\
innodb_large_prefix\n\
innodb_flush_log_at_trx_commit=2\n\
sync_binlog=0\n\
innodb_use_native_aio=0\n\
sql_mode=TRADITIONAL,NO_AUTO_VALUE_ON_ZERO,ONLY_FULL_GROUP_BY\n\
' >> /etc/mysql/my.cnf
